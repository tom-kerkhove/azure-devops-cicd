﻿using System;
using System.Threading.Tasks;
using Bogus;
using Demo.Bitbucket.Workload.Model;

namespace Demo.Bitbucket.Workload
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            while (true)
            {
                var order = GenerateOrder();

                Console.WriteLine("Processing order {0} for {1} units of {2} bought by {3} {4}", order.Id, order.Amount, order.ArticleNumber, order.Customer.FirstName, order.Customer.LastName);


                await Task.Delay(TimeSpan.FromSeconds(2));
            }
        }

        private static Order GenerateOrder()
        {
            var customerGenerator = new Faker<Customer>()
                .RuleFor(u => u.FirstName, (f, u) => f.Name.FirstName())
                .RuleFor(u => u.LastName, (f, u) => f.Name.LastName());

            var orderGenerator = new Faker<Order>()
                .RuleFor(u => u.Customer, () => customerGenerator)
                .RuleFor(u => u.Id, f => Guid.NewGuid().ToString())
                .RuleFor(u => u.Amount, f => f.Random.Int())
                .RuleFor(u => u.ArticleNumber, f => f.Commerce.Product());

            return orderGenerator.Generate();
        }
    }
}