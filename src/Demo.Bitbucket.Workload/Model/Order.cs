﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demo.Bitbucket.Workload.Model
{
    public class Order
    {
        public string Id { get; set; }
        public int Amount { get; set; }
        public string ArticleNumber { get; set; }
        public Customer Customer { get; set; }
    }
}
