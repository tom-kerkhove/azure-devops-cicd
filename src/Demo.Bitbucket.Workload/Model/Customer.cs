﻿namespace Demo.Bitbucket.Workload.Model
{
    public class Customer
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}