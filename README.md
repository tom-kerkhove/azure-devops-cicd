# Azure DevOps - CI/CD Demo

This repo contains the source code for Azure DevOps CI/CD demo with release tracking.

You can find all the pipelines [here](https://dev.azure.com/tomkerkhove/Bitbucket%20Integration).

## Scenarios

### Validating Pull Requests

![Validating PRs](./media/validating-prs.png)

### Deploying Azure Functions to Azure

Here is an overview of how to deploy Azure Functions to Azure:

![Deploying Azure Functions to Azure](./media/deploy-function-to-azure.png)

### Deploying containers on Kubernetes

Here is an overview of how to deploy to Kubernetes:

![Deploying containers on Kubernetes](./media/deploy-container-to-azure-cluster-per-env.png)

As you can see, every deployment pipeline is automatically triggered and by using Environments it enforces approval before it gets shipped to new environments:

![Deployment example](./media/deploy-container-pipeline.png)

However, you can share a cluster to run NON-PROD workloads and reduce the cost by providing a Kubernetes namespace per environment.

When this approach is used, RBAC should be enabled to lock the cluster down and resource limitations should be used to avoid flooding other environments.

![Deploying containers on Kubernetes with NON-Prod cluster](./media/deploy-container-to-azure-cluster-for-non-prod.png)

## Tracebility & Control

When using Azure Pipeline Environments they give you tracability & control over what is being deployed and whom gets to approve it.

![Environment Overview](./media/environment-overview.png)
![Environment info for deployments](./media/environment-overview-deployments.png)
![Environment info for cluster](./media/environment-overview-for-cluster.png)
![Deployment Traceabilty & Status](./media/environment-deployment-traceability-and-status.png)
![Pod info](./media/environment-pod-info.png)
![Pod logs](./media/environment-pod-logs.png)
